package demo.aop.monitor;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.message.AsynchronouslyFormattable;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;


@Slf4j
@Component
@Aspect
//切面类，重复边缘的事情交给中介做
/**
 * 这段代码是一个使用Spring AOP实现的切面类，主要用于在房屋租赁服务中的一些操作前后添加额外的逻辑。下面是代码的中文解释：
 *
 * 1.  `@Component` 注解表示这是一个Spring组件，会被Spring容器管理。
 * 2.  `@Aspect` 注解表示这是一个切面类，用于定义切点和通知。
 * 3.  `@Value("${agent:某某中介}")` 注解用于注入配置文件中的属性值，这里注入了一个名为"agent"的属性，默认值为"某某中介"。
 * 4.  `@Pointcut("execution(* demo.aop.service.Landlord.service())")` 定义了一个切点，表示匹配 `demo.aop.service.Landlord` 类中的 `service()` 方法。
 * 5.  `@Before("IService()")` 定义了一个前置通知，在切点 `IService()` 执行前执行，输出中介带租客看房和谈价格的操作。
 * 6.  `@After("IService()")` 定义了一个后置通知，在切点 `IService()` 执行后执行，输出中介交钥匙的操作。
 *
 * 这段代码的作用是在房屋租赁服务中，通过AOP的方式在指定方法执行前后添加中介相关的操作，例如带租客看房、谈价格和交钥匙。
 */
public class Agent {
    @Value("${agent:某某中介}")
    private String agent;

    /*
    //定义了一个切点，表示匹配 demo.aop.service.Landlord 类中的 service() 方法。
    @Pointcut("execution(* demo.aop.service.Landlord.service())")
    public void IService() {
    }

    @Before("IService()")
    public void before() {
        System.out.println(agent + "带租客看房");
        System.out.println(agent + "谈价格");
    }

    @After("IService()")
    public void after() {
        System.out.println(agent + "交钥匙");
    }
    */


    //表示一个切点表达式，用于匹配带有特定注解 demo.aop.monitor.QmTest 的方法
    @Pointcut("@annotation(demo.aop.monitor.QmTest)")
    public void servicePointCut() {

    }

    /*使用 @Around 注解来同时完成前置和后置通知*/
    @Around("servicePointCut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        // 请求对象
        Object[] args = joinPoint.getArgs();
        log.info(Arrays.toString(args));
        System.out.println("带租客看房");
        System.out.println("谈价格");
        // 结果此处表示执行注解下的方法
        Object result = joinPoint.proceed();
        // 获取方法连接点的相关信息，获取注解传入内容 开始
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String methodName = methodSignature.getName();
        Method method = methodSignature.getMethod();
        QmTest qmTest = method.getAnnotation(QmTest.class);
        String str = qmTest.str();
        long end = System.currentTimeMillis();
        // 获取方法连接点的相关信息，获取注解传入内容 结束
        System.out.println("交钥匙");

        log.info("传入内容: {}", str);
        log.info("请求参数: {} ", Arrays.toString(args));
        log.info("耗时: {} ms", end - start);
        log.info("请求方法: {} ", methodName);
        log.info("返回结果: {} ", Objects.isNull(result) ? "" : result);
    }
}
