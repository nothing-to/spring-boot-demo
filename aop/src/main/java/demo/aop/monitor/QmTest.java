package demo.aop.monitor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
// 定义一个注解类，用于标记
public @interface QmTest {

    /**
    * 设置一个注解传入的值
    */
    String str();
}
