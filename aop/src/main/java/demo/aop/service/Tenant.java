package demo.aop.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/***
 * 租客
 */
@Component
public class Tenant {

    @Value("${landlord1:某租客}")
    private String name;

    public void service() {
        System.out.println(name + "负责签合同");
        System.out.println(name + "负责交房租");
    }
}
