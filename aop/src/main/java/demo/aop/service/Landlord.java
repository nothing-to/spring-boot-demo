package demo.aop.service;

import demo.aop.monitor.QmTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
// 房东只要关心自己的核心业务功能
public class Landlord {

    @Autowired
    private Tenant tenant;

    @Value("${landlord:某某}")
    private String landlord;

    @QmTest(str="测试aop")
    public void service() {
        System.out.println(landlord + "负责签合同");
        tenant.service();
        System.out.println(landlord + "负责收房租");
    }
}