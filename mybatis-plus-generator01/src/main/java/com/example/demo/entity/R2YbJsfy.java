package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医保结算费用分类信息表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_JSFY")
@ApiModel(value="R2YbJsfy对象", description="医保结算费用分类信息表")
public class R2YbJsfy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "医保结算ID")
    @TableField("JSHID")
    private String JSHID;

    @ApiModelProperty(value = "超过限价金额")
    @TableField("CGXJJE")
    private BigDecimal CGXJJE;

    @ApiModelProperty(value = "药品费用")
    @TableField("YPFY")
    private BigDecimal YPFY;

    @ApiModelProperty(value = "西药费用")
    @TableField("XYFY")
    private BigDecimal XYFY;

    @ApiModelProperty(value = "中药费用")
    @TableField("ZYFY")
    private BigDecimal ZYFY;

    @ApiModelProperty(value = "中成药费用")
    @TableField("ZCYFY")
    private BigDecimal ZCYFY;

    @ApiModelProperty(value = "检验检查费用")
    @TableField("JCJYFY")
    private BigDecimal JCJYFY;

    @ApiModelProperty(value = "一次性材料费用")
    @TableField("YCXCLFY")
    private BigDecimal YCXCLFY;


}
