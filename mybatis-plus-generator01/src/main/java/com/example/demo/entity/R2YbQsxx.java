package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医保清算信息表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_QSXX")
@ApiModel(value="R2YbQsxx对象", description="医保清算信息表")
public class R2YbQsxx implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "清算区划")
    @TableField("QSQH")
    private String QSQH;

    @ApiModelProperty(value = "人群类别(职工,居民)")
    @TableField("RQLB")
    private String RQLB;

    @ApiModelProperty(value = "清算年月")
    @TableField("QSNY")
    private String QSNY;

    @ApiModelProperty(value = "医疗统筹类别范围")
    @TableField("MEDINSURTYPE")
    private String MEDINSURTYPE;

    @ApiModelProperty(value = "总金额")
    @TableField("ZJE")
    private BigDecimal ZJE;

    @ApiModelProperty(value = "稽核扣款金额")
    @TableField("AUDT_DET_AMT")
    private BigDecimal AUDT_DET_AMT;

    @ApiModelProperty(value = "基金扣款金额")
    @TableField("FUND_DET_AMT")
    private BigDecimal FUND_DET_AMT;

    @ApiModelProperty(value = "结算审核扣款金额")
    @TableField("SETL_CHK_DET_AMT")
    private BigDecimal SETL_CHK_DET_AMT;

    @ApiModelProperty(value = "预留保证金总额")
    @TableField("DPST_SUMAMT")
    private BigDecimal DPST_SUMAMT;

    @ApiModelProperty(value = "定额奖励金额")
    @TableField("QUOT_RW_AMT")
    private BigDecimal QUOT_RW_AMT;

    @ApiModelProperty(value = "定额扣除金额")
    @TableField("QUOT_DET_AMT")
    private BigDecimal QUOT_DET_AMT;

    @ApiModelProperty(value = "清算金额")
    @TableField("QSJE")
    private BigDecimal QSJE;


}
