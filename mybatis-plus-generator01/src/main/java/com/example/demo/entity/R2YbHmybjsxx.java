package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 惠民保结算信息
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_HMYBJSXX")
@ApiModel(value="R2YbHmybjsxx对象", description="惠民保结算信息")
public class R2YbHmybjsxx implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "病人结算号")
    @TableField("JSHID")
    private String JSHID;

    @ApiModelProperty(value = "省异地个人账户支付")
    @TableField("SYDGRZHZF")
    private BigDecimal SYDGRZHZF;

    @ApiModelProperty(value = "惠民保报销金额目录内")
    @TableField("HMBBXJEMLN")
    private BigDecimal HMBBXJEMLN;

    @ApiModelProperty(value = "惠民保报销金额目录外")
    @TableField("HMBBXJEMLW")
    private BigDecimal HMBBXJEMLW;

    @ApiModelProperty(value = "惠民保报销金额")
    @TableField("HMBBXJE")
    private BigDecimal HMBBXJE;


}
