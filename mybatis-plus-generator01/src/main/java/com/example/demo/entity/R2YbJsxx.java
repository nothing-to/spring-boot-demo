package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医保结算信息表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_JSXX")
@ApiModel(value="R2YbJsxx对象", description="医保结算信息表")
public class R2YbJsxx implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统筹登记号")
    @TableField("YBJZDJNO")
    private String YBJZDJNO;

    @ApiModelProperty(value = "医保结算ID")
    @TableField("YBJSID")
    private String YBJSID;

    @ApiModelProperty(value = "住院流水号")
    @TableField("HISJZLSH")
    private String HISJZLSH;

    @ApiModelProperty(value = "住院方式")
    @TableField("ZYFS")
    private String ZYFS;

    @ApiModelProperty(value = "科室编码")
    @TableField("DEPTCODE")
    private String DEPTCODE;

    @ApiModelProperty(value = "科室名称")
    @TableField("DEPTNAME")
    private String DEPTNAME;

    @ApiModelProperty(value = "医疗统筹类别")
    @TableField("TCLB")
    private String TCLB;

    @ApiModelProperty(value = "医疗统筹类别明细")
    @TableField("YLTCLBMX")
    private String YLTCLBMX;

    @ApiModelProperty(value = "身份证号")
    @TableField("SFZH")
    private String SFZH;

    @ApiModelProperty(value = "社会保障号码")
    @TableField("SHBZH")
    private String SHBZH;

    @ApiModelProperty(value = "参保地编码")
    @TableField("CBDBM")
    private String CBDBM;

    @ApiModelProperty(value = "参保地名称")
    @TableField("CBD")
    private String CBD;

    @ApiModelProperty(value = "是否省外")
    @TableField("ISSW")
    private String ISSW;

    @ApiModelProperty(value = "参保类型（职工、居民）")
    @TableField("CBLX")
    private String CBLX;

    @ApiModelProperty(value = "姓名")
    @TableField("XM")
    private String XM;

    @ApiModelProperty(value = "性别")
    @TableField("XB")
    private String XB;

    @ApiModelProperty(value = "职工类别")
    @TableField("ZGLB")
    private String ZGLB;

    @ApiModelProperty(value = "就医类别")
    @TableField("JYLB")
    private String JYLB;

    @ApiModelProperty(value = "入院日期")
    @TableField("RYRQ")
    private LocalDateTime RYRQ;

    @ApiModelProperty(value = "出院日期")
    @TableField("CYRQ")
    private LocalDateTime CYRQ;

    @ApiModelProperty(value = "结算日期")
    @TableField("JSRQ")
    private LocalDateTime JSRQ;

    @ApiModelProperty(value = "住院天数")
    @TableField("ZYTS")
    private BigDecimal ZYTS;

    @ApiModelProperty(value = "疾病编码")
    @TableField("JBBM")
    private String JBBM;

    @ApiModelProperty(value = "疾病名称")
    @TableField("JBMC")
    private String JBMC;

    @ApiModelProperty(value = "出院诊断")
    @TableField("CYZD")
    private String CYZD;

    @ApiModelProperty(value = "经办机构")
    @TableField("JBJG")
    private String JBJG;

    @ApiModelProperty(value = "上报日期")
    @TableField("SBRQ")
    private LocalDateTime SBRQ;

    @ApiModelProperty(value = "操作员")
    @TableField("CZY")
    private String CZY;

    @ApiModelProperty(value = "总金额")
    @TableField("ZJE")
    private BigDecimal ZJE;

    @ApiModelProperty(value = "病人负担金额")
    @TableField("BRFDJE")
    private BigDecimal BRFDJE;

    @ApiModelProperty(value = "医保负担金额")
    @TableField("YBFDJE")
    private BigDecimal YBFDJE;

    @ApiModelProperty(value = "医疗补助金额")
    @TableField("YLBZJE")
    private BigDecimal YLBZJE;

    @ApiModelProperty(value = "医院负担金额")
    @TableField("YYFDJE")
    private BigDecimal YYFDJE;

    @ApiModelProperty(value = "个人账户")
    @TableField("GRZHZF")
    private BigDecimal GRZHZF;

    @ApiModelProperty(value = "基本统筹支付")
    @TableField("JBTCZF")
    private BigDecimal JBTCZF;

    @ApiModelProperty(value = "大额支付")
    @TableField("DEZF")
    private BigDecimal DEZF;

    @ApiModelProperty(value = "公务员补助")
    @TableField("GWYBZ")
    private BigDecimal GWYBZ;

    @ApiModelProperty(value = "本次起付线")
    @TableField("BCQFX")
    private BigDecimal BCQFX;

    @ApiModelProperty(value = "本次纳入统筹范围")
    @TableField("BCNRTCFW")
    private BigDecimal BCNRTCFW;

    @ApiModelProperty(value = "本年纳入统筹范围")
    @TableField("BNYNRTCFW")
    private BigDecimal BNYNRTCFW;

    @ApiModelProperty(value = "大病补助金额")
    @TableField("DBBZJE")
    private BigDecimal DBBZJE;

    @ApiModelProperty(value = "特惠保金额")
    @TableField("PKRKBCBXJE")
    private BigDecimal PKRKBCBXJE;

    @ApiModelProperty(value = "民政补助")
    @TableField("MZBZ")
    private BigDecimal MZBZ;

    @ApiModelProperty(value = "部分自付")
    @TableField("BFZF")
    private BigDecimal BFZF;

    @ApiModelProperty(value = "全额自费")
    @TableField("QEZF")
    private BigDecimal QEZF;

    @ApiModelProperty(value = "平均床日费用")
    @TableField("PJCRF")
    private BigDecimal PJCRF;

    @ApiModelProperty(value = "民政优抚")
    @TableField("MZYF")
    private BigDecimal MZYF;

    @ApiModelProperty(value = "暂缓统筹金")
    @TableField("ZHZF")
    private BigDecimal ZHZF;

    @ApiModelProperty(value = "其他统筹支付")
    @TableField("QTTCZF")
    private BigDecimal QTTCZF;

    @ApiModelProperty(value = "职工大额二次报销")
    @TableField("ZGDEECBX")
    private BigDecimal ZGDEECBX;

    @ApiModelProperty(value = "现金支付")
    @TableField("XJZF")
    private BigDecimal XJZF;

    @ApiModelProperty(value = "贫困人员再救助金额")
    @TableField("PKRYZJZJE")
    private BigDecimal PKRYZJZJE;

    @ApiModelProperty(value = "对账标记")
    @TableField("DZFLAG")
    private BigDecimal DZFLAG;


}
