package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医保对账表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_DZB")
@ApiModel(value="R2YbDzb对象", description="医保对账表")
public class R2YbDzb implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "序号")
    @TableField("KID")
    private BigDecimal KID;

    @ApiModelProperty(value = "起始日期")
    @TableField("BEGDATE")
    private LocalDateTime BEGDATE;

    @ApiModelProperty(value = "结束日期")
    @TableField("ENDDATE")
    private LocalDateTime ENDDATE;

    @ApiModelProperty(value = "对账类别")
    @TableField("DZLB")
    private String DZLB;

    @ApiModelProperty(value = "结算人次")
    @TableField("JSRC")
    private BigDecimal JSRC;

    @ApiModelProperty(value = "总金额")
    @TableField("ZJE")
    private BigDecimal ZJE;

    @ApiModelProperty(value = "统筹金额")
    @TableField("TCJE")
    private BigDecimal TCJE;

    @ApiModelProperty(value = "个人账户支付")
    @TableField("GRZHZF")
    private BigDecimal GRZHZF;

    @ApiModelProperty(value = "现金支付")
    @TableField("XJZF")
    private BigDecimal XJZF;

    @ApiModelProperty(value = "对账人")
    @TableField("DZR")
    private String DZR;

    @ApiModelProperty(value = "对账日期")
    @TableField("DZRQ")
    private LocalDateTime DZRQ;

    @ApiModelProperty(value = "审核人")
    @TableField("SHR")
    private String SHR;

    @ApiModelProperty(value = "审核日期")
    @TableField("SHRQ")
    private LocalDateTime SHRQ;

    @ApiModelProperty(value = "对账状态")
    @TableField("DZZT")
    private String DZZT;


}
