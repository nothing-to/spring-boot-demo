package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * DRG分组表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_DRG")
@ApiModel(value="R2YbDrg对象", description="DRG分组表")
public class R2YbDrg implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统筹登记号")
    @TableField("YBTJDJNO")
    private String YBTJDJNO;

    @ApiModelProperty(value = "人员大类(职工、居民)")
    @TableField("RYDL")
    private String RYDL;

    @ApiModelProperty(value = "出院科室名称")
    @TableField("CYKS")
    private String CYKS;

    @ApiModelProperty(value = "就诊流水号")
    @TableField("JZLSH")
    private String JZLSH;

    @ApiModelProperty(value = "身份证号")
    @TableField("SFZH")
    private String SFZH;

    @ApiModelProperty(value = "姓名")
    @TableField("XM")
    private String XM;

    @ApiModelProperty(value = "入院日期")
    @TableField("RYRQ")
    private LocalDateTime RYRQ;

    @ApiModelProperty(value = "出院日期")
    @TableField("CYRQ")
    private LocalDateTime CYRQ;

    @ApiModelProperty(value = "结算日期")
    @TableField("JSRQ")
    private LocalDateTime JSRQ;

    @ApiModelProperty(value = "床日(住院天数)")
    @TableField("TS")
    private BigDecimal TS;

    @ApiModelProperty(value = "主诊断编码")
    @TableField("ZYZDBM")
    private String ZYZDBM;

    @ApiModelProperty(value = "主诊断名称")
    @TableField("ZYZDMC")
    private String ZYZDMC;

    @ApiModelProperty(value = "医疗总费用")
    @TableField("ZJE")
    private BigDecimal ZJE;

    @ApiModelProperty(value = "基本医疗费用")
    @TableField("YBJBYLFY")
    private BigDecimal YBJBYLFY;

    @ApiModelProperty(value = "个人自付")
    @TableField("GRZF")
    private BigDecimal GRZF;

    @ApiModelProperty(value = "DRG编码")
    @TableField("DRGCODE")
    private String DRGCODE;

    @ApiModelProperty(value = "DRG名称")
    @TableField("DRGNAME")
    private String DRGNAME;

    @ApiModelProperty(value = "支付方式")
    @TableField("PAYWAY")
    private String PAYWAY;

    @ApiModelProperty(value = "权重")
    @TableField("QZ")
    private BigDecimal QZ;

    @ApiModelProperty(value = "费率")
    @TableField("FL")
    private BigDecimal FL;

    @ApiModelProperty(value = "支付标准")
    @TableField("ZFBZ")
    private BigDecimal ZFBZ;

    @ApiModelProperty(value = "DRG支付金额")
    @TableField("DRGZFJE")
    private BigDecimal DRGZFJE;

    @ApiModelProperty(value = "清算金额")
    @TableField("QSJE")
    private BigDecimal QSJE;

    @ApiModelProperty(value = "清算原则")
    @TableField("QSYZ")
    private String QSYZ;

    @ApiModelProperty(value = "是否延迟结算(是，否)")
    @TableField("ISYCQS")
    private String ISYCQS;

    @ApiModelProperty(value = "DRG分组类别(低费用病例,歧义组病例)")
    @TableField("DRGFZLB")
    private String DRGFZLB;


}
