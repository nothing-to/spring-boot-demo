package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医保对账明细表
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("R2_YB_DZMX")
@ApiModel(value="R2YbDzmx对象", description="医保对账明细表")
public class R2YbDzmx implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "对账序号")
    @TableField("DZ_KID")
    private BigDecimal DZ_KID;

    @ApiModelProperty(value = "明细序号")
    @TableField("XH")
    private BigDecimal XH;

    @ApiModelProperty(value = "医保登记ID")
    @TableField("YBDJNO")
    private String YBDJNO;

    @ApiModelProperty(value = "医保结算ID")
    @TableField("JSID")
    private String JSID;

    @ApiModelProperty(value = "HIS就诊流水号")
    @TableField("HIS_LSH")
    private String HIS_LSH;

    @ApiModelProperty(value = "对账状态")
    @TableField("DZZT")
    private String DZZT;


}
