package com.example.demo.mapper;

import com.example.demo.entity.R2YbDrg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * DRG分组表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbDrgMapper extends BaseMapper<R2YbDrg> {

}
