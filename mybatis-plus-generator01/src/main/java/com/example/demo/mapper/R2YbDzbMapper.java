package com.example.demo.mapper;

import com.example.demo.entity.R2YbDzb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保对账表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbDzbMapper extends BaseMapper<R2YbDzb> {

}
