package com.example.demo.mapper;

import com.example.demo.entity.R2YbDzmx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保对账明细表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbDzmxMapper extends BaseMapper<R2YbDzmx> {

}
