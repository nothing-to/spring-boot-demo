package com.example.demo.mapper;

import com.example.demo.entity.R2YbQsbfjl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保拨付记录表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbQsbfjlMapper extends BaseMapper<R2YbQsbfjl> {

}
