package com.example.demo.mapper;

import com.example.demo.entity.R2YbQsxx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保清算信息表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbQsxxMapper extends BaseMapper<R2YbQsxx> {

}
