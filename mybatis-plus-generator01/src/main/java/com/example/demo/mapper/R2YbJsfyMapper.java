package com.example.demo.mapper;

import com.example.demo.entity.R2YbJsfy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保结算费用分类信息表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbJsfyMapper extends BaseMapper<R2YbJsfy> {

}
