package com.example.demo.mapper;

import com.example.demo.entity.R2YbHmybjsxx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 惠民保结算信息 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbHmybjsxxMapper extends BaseMapper<R2YbHmybjsxx> {

}
