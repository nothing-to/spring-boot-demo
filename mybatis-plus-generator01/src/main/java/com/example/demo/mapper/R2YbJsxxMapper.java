package com.example.demo.mapper;

import com.example.demo.entity.R2YbJsxx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医保结算信息表 Mapper 接口
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface R2YbJsxxMapper extends BaseMapper<R2YbJsxx> {

}
