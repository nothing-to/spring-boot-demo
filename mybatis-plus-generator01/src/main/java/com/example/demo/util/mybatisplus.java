package com.example.demo.util;


import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.List;

/**
 * @author wangzhe
 * @NAME: mybatisplus
 * @DATE: 2021/12/16
 */
public class mybatisplus {

    /**
     * 启动生成代码
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("------开始---------");
        doGenerator();
        System.out.println("------结束---------");
    }

    /**
     * 基础配置
     */
    private static String outputDir = System.getProperty("user.dir") + "/mybatis-plus-generator01/src/main/java";
    private static String author = "wz";
    /**
     * 数据库配置
     */
    private static DbType dbType = DbType.ORACLE;
    private static String driverName = "oracle.jdbc.driver.OracleDriver";
    private static String userName = "ihd";
    private static String password = "IHD";
    private static String url = "jdbc:oracle:thin:@//192.168.10.154:1521/bhis";
    // private static String url = "jdbc:oracle:thin:@//192.168.10.154:1521/bhis?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8";
    private static String[] tables = {"R2_YB_DRG","R2_YB_DZB"};
    /**
     * 生成包路径
     */
    private static String packageParent = "com.duizhang.demo";
    private static String entity = "entity";
    private static String mapper = "dao";
    private static String mapperXml = "mapper";
    private static String service = "service";
    private static String serviceImpl = "service.impl";
    private static String controller = "controller";

    public static void doGenerator() {
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //代码生成存放位置
        gc.setOutputDir(outputDir);
        gc.setFileOverride(true);
        gc.setActiveRecord(false);
        gc.setEnableCache(false);
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(false);
        gc.setOpen(true);
        gc.setAuthor(author);
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceImplName("%sService");
        gc.setServiceName("I%sService");
        gc.setControllerName("%sController");
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(dbType);
        dsc.setDriverName(driverName);
        dsc.setUsername(userName);
        dsc.setPassword(password);
        dsc.setUrl(url);
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setInclude(tables);
        strategy.setSuperEntityColumns(new String[]{});
        //strategy.setSuperMapperClass("com.baomidou.mybatisplus.core.mapper.BaseMapper");
        List<TableFill> tableFillList = CollUtil.newArrayList();
        TableFill fill = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        tableFillList.add(fill);
        fill = new TableFill("create_time", FieldFill.INSERT);
        tableFillList.add(fill);
        strategy.setTableFillList(tableFillList);
        mpg.setStrategy(strategy);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(packageParent);
        // 代码生成包路径
        pc.setEntity(entity);
        pc.setMapper(mapper);
        pc.setXml(mapperXml);
        pc.setService(service);
        pc.setServiceImpl(serviceImpl);
        pc.setController(controller);
        mpg.setPackageInfo(pc);
        // 注入自定义配置，可以在 VM 中使用 ${cfg.packageMy} 设置值
        // InjectionConfig cfg = new InjectionConfig() {
        //     public void initMap() {
        //         Map<String, Object> map = new HashMap<String, Object>();
        //         map.put("packageMy", packageBase);
        //         this.setMap(map);
        //     }
        // };

        // mpg.setCfg(cfg);

         TemplateConfig tc = new TemplateConfig();
         tc.setEntity("templates/entity.java.vm");
         tc.setMapper("templates/mapper.java.vm");
         tc.setXml("templates/mapper.xml.vm");
         tc.setServiceImpl("templates/serviceImpl.java.vm");
         tc.setService("templates/service.java.vm");
         tc.setController("templates/controller.java.vm");
        // mpg.setTemplate(tc);
        // 执行生成
        mpg.execute();
    }
}