package com.example.demo.service.impl;

import com.example.demo.entity.R2YbDzb;
import com.example.demo.mapper.R2YbDzbMapper;
import com.example.demo.service.IR2YbDzbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保对账表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbDzbServiceImpl extends ServiceImpl<R2YbDzbMapper, R2YbDzb> implements IR2YbDzbService {

}
