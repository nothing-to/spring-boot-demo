package com.example.demo.service;

import com.example.demo.entity.R2YbHmybjsxx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 惠民保结算信息 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbHmybjsxxService extends IService<R2YbHmybjsxx> {

}
