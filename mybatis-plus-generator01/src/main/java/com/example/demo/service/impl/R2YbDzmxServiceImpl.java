package com.example.demo.service.impl;

import com.example.demo.entity.R2YbDzmx;
import com.example.demo.mapper.R2YbDzmxMapper;
import com.example.demo.service.IR2YbDzmxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保对账明细表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbDzmxServiceImpl extends ServiceImpl<R2YbDzmxMapper, R2YbDzmx> implements IR2YbDzmxService {

}
