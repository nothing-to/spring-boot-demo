package com.example.demo.service.impl;

import com.example.demo.entity.R2YbJsfy;
import com.example.demo.mapper.R2YbJsfyMapper;
import com.example.demo.service.IR2YbJsfyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保结算费用分类信息表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbJsfyServiceImpl extends ServiceImpl<R2YbJsfyMapper, R2YbJsfy> implements IR2YbJsfyService {

}
