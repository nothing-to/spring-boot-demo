package com.example.demo.service.impl;

import com.example.demo.entity.R2YbHmybjsxx;
import com.example.demo.mapper.R2YbHmybjsxxMapper;
import com.example.demo.service.IR2YbHmybjsxxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 惠民保结算信息 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbHmybjsxxServiceImpl extends ServiceImpl<R2YbHmybjsxxMapper, R2YbHmybjsxx> implements IR2YbHmybjsxxService {

}
