package com.example.demo.service;

import com.example.demo.entity.R2YbDzmx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医保对账明细表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbDzmxService extends IService<R2YbDzmx> {

}
