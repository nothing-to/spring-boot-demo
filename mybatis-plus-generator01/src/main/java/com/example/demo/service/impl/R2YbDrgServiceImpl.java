package com.example.demo.service.impl;

import com.example.demo.entity.R2YbDrg;
import com.example.demo.mapper.R2YbDrgMapper;
import com.example.demo.service.IR2YbDrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * DRG分组表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbDrgServiceImpl extends ServiceImpl<R2YbDrgMapper, R2YbDrg> implements IR2YbDrgService {

}
