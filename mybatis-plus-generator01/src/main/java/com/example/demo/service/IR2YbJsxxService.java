package com.example.demo.service;

import com.example.demo.entity.R2YbJsxx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医保结算信息表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbJsxxService extends IService<R2YbJsxx> {

}
