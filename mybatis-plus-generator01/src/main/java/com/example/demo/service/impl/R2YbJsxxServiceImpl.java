package com.example.demo.service.impl;

import com.example.demo.entity.R2YbJsxx;
import com.example.demo.mapper.R2YbJsxxMapper;
import com.example.demo.service.IR2YbJsxxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保结算信息表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbJsxxServiceImpl extends ServiceImpl<R2YbJsxxMapper, R2YbJsxx> implements IR2YbJsxxService {

}
