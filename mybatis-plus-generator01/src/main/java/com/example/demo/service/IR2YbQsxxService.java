package com.example.demo.service;

import com.example.demo.entity.R2YbQsxx;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医保清算信息表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbQsxxService extends IService<R2YbQsxx> {

}
