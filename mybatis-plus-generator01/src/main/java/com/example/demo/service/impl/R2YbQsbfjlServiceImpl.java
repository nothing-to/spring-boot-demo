package com.example.demo.service.impl;

import com.example.demo.entity.R2YbQsbfjl;
import com.example.demo.mapper.R2YbQsbfjlMapper;
import com.example.demo.service.IR2YbQsbfjlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保拨付记录表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbQsbfjlServiceImpl extends ServiceImpl<R2YbQsbfjlMapper, R2YbQsbfjl> implements IR2YbQsbfjlService {

}
