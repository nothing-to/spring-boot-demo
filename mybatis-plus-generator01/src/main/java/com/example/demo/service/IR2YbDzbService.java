package com.example.demo.service;

import com.example.demo.entity.R2YbDzb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医保对账表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbDzbService extends IService<R2YbDzb> {

}
