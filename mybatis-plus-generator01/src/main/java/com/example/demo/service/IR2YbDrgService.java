package com.example.demo.service;

import com.example.demo.entity.R2YbDrg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * DRG分组表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbDrgService extends IService<R2YbDrg> {

}
