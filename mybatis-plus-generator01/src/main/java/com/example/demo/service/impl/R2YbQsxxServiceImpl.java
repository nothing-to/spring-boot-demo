package com.example.demo.service.impl;

import com.example.demo.entity.R2YbQsxx;
import com.example.demo.mapper.R2YbQsxxMapper;
import com.example.demo.service.IR2YbQsxxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医保清算信息表 服务实现类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@Service
public class R2YbQsxxServiceImpl extends ServiceImpl<R2YbQsxxMapper, R2YbQsxx> implements IR2YbQsxxService {

}
