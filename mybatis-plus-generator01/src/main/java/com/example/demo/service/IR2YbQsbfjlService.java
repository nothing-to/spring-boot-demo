package com.example.demo.service;

import com.example.demo.entity.R2YbQsbfjl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医保拨付记录表 服务类
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
public interface IR2YbQsbfjlService extends IService<R2YbQsbfjl> {

}
