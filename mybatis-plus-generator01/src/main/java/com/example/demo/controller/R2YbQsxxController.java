package com.example.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import com.example.demo.util.Result;
import java.util.Arrays;

    import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.IR2YbQsxxService;
import com.example.demo.entity.R2YbQsxx;
import javax.validation.Valid;

/**
 * <p>
 * 医保清算信息表 前端控制器
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/r2-yb-qsxx")
@Api(value = "医保清算信息表", tags = "医保清算信息表接口")
    public class R2YbQsxxController {

private final IR2YbQsxxService r2YbQsxxService;



/**
 * 医保清算信息表信息
 *
 * @param id Id
 * @return Result
 */
@GetMapping("/get")
@ApiOperation(value = "医保清算信息表信息", notes = "根据ID查询")
@ApiImplicitParams({
        @ApiImplicitParam(name = "id", required = true, value = "ID", paramType = "form"),
})
public Result<?> get(@RequestParam String id) {
        return Result.success(r2YbQsxxService.getById(id));
        }

/**
 * 医保清算信息表设置
 *
 * @param r2YbQsxx R2YbQsxx 对象
 * @return Result
 */
@PostMapping("/set")
@ApiOperation(value = "医保清算信息表设置", notes = "医保清算信息表设置,支持新增或修改")
public Result<?> set(@Valid @RequestBody R2YbQsxx r2YbQsxx) {
        return Result.success(r2YbQsxxService.saveOrUpdate(r2YbQsxx));
        }

/**
 * 医保清算信息表删除
 *
 * @param ids id字符串，根据,号分隔
 * @return Result
 */
@PostMapping("/del")
@ApiOperation(value = "医保清算信息表删除", notes = "医保清算信息表删除")
@ApiImplicitParams({
        @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form")
})
public Result<?> del(@RequestParam String ids) {
        return Result.success(r2YbQsxxService.removeByIds(Arrays.asList (ids.split (","))));
        }
        }

