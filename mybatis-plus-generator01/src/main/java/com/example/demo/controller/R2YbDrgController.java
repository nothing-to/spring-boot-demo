package com.example.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import com.example.demo.util.Result;

import java.util.Arrays;

import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.IR2YbDrgService;
import com.example.demo.entity.R2YbDrg;

import javax.validation.Valid;

/**
 * <p>
 * DRG分组表 前端控制器
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/r2-yb-drg")
@Api(value = "DRG分组表", tags = "DRG分组表接口")
public class R2YbDrgController {

    private final IR2YbDrgService r2YbDrgService;


    /**
     * DRG分组表信息
     *
     * @param id Id
     * @return Result
     */
    @GetMapping("/get")
    @ApiOperation(value = "DRG分组表信息", notes = "根据ID查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", required = true, value = "ID", paramType = "form"),
    })
    public Result<?> get(@RequestParam String id) {
        return Result.success(r2YbDrgService.getById(id));
    }

    /**
     * DRG分组表设置
     *
     * @param r2YbDrg R2YbDrg 对象
     * @return Result
     */
    @PostMapping("/set")
    @ApiOperation(value = "DRG分组表设置", notes = "DRG分组表设置,支持新增或修改")
    public Result<?> set(@Valid @RequestBody R2YbDrg r2YbDrg) {
        return Result.success(r2YbDrgService.saveOrUpdate(r2YbDrg));
    }

    /**
     * DRG分组表删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @PostMapping("/del")
    @ApiOperation(value = "DRG分组表删除", notes = "DRG分组表删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form")
    })
    public Result<?> del(@RequestParam String ids) {
        return Result.success(r2YbDrgService.removeByIds(Arrays.asList(ids.split(","))));
    }
}

