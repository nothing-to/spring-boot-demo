package com.example.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import com.example.demo.util.Result;
import java.util.Arrays;

    import org.springframework.web.bind.annotation.RestController;
import com.example.demo.service.IR2YbHmybjsxxService;
import com.example.demo.entity.R2YbHmybjsxx;
import javax.validation.Valid;

/**
 * <p>
 * 惠民保结算信息 前端控制器
 * </p>
 *
 * @author wz
 * @since 2023-09-14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/r2-yb-hmybjsxx")
@Api(value = "惠民保结算信息", tags = "惠民保结算信息接口")
    public class R2YbHmybjsxxController {

private final IR2YbHmybjsxxService r2YbHmybjsxxService;



/**
 * 惠民保结算信息信息
 *
 * @param id Id
 * @return Result
 */
@GetMapping("/get")
@ApiOperation(value = "惠民保结算信息信息", notes = "根据ID查询")
@ApiImplicitParams({
        @ApiImplicitParam(name = "id", required = true, value = "ID", paramType = "form"),
})
public Result<?> get(@RequestParam String id) {
        return Result.success(r2YbHmybjsxxService.getById(id));
        }

/**
 * 惠民保结算信息设置
 *
 * @param r2YbHmybjsxx R2YbHmybjsxx 对象
 * @return Result
 */
@PostMapping("/set")
@ApiOperation(value = "惠民保结算信息设置", notes = "惠民保结算信息设置,支持新增或修改")
public Result<?> set(@Valid @RequestBody R2YbHmybjsxx r2YbHmybjsxx) {
        return Result.success(r2YbHmybjsxxService.saveOrUpdate(r2YbHmybjsxx));
        }

/**
 * 惠民保结算信息删除
 *
 * @param ids id字符串，根据,号分隔
 * @return Result
 */
@PostMapping("/del")
@ApiOperation(value = "惠民保结算信息删除", notes = "惠民保结算信息删除")
@ApiImplicitParams({
        @ApiImplicitParam(name = "ids", required = true, value = "多个用,号隔开", paramType = "form")
})
public Result<?> del(@RequestParam String ids) {
        return Result.success(r2YbHmybjsxxService.removeByIds(Arrays.asList (ids.split (","))));
        }
        }

