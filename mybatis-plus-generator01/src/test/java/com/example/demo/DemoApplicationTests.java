// package com.example.demo;
//
// import com.baomidou.mybatisplus.core.toolkit.Assert;
// import com.example.demo.entity.User;
// import com.example.demo.mapper.UserMapper;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
//
// import java.util.List;
//
// @SpringBootTest
// class DemoApplicationTests {
//
//     @Test
//     void contextLoads() {
//     }
//
//     @Autowired
//     private UserMapper userMapper;
//
//     @Test
//     public void testSelect() {
//         System.out.println(("----- selectAll method test ------"));
//         List<User> userList = userMapper.selectList(null);
//         Assert.isTrue(5 == userList.size(), "");
//         userList.forEach(System.out::println);
//     }
// }
