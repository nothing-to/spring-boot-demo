package com.bkhip.msg;

import com.bkhip.msg.body.CriticalValueAddBody;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author biver
 * @version 1.0
 * @description 新增危急值消息类
 * @date 2023/8/17 17:03:32
 */
@XmlAccessorType(XmlAccessType.FIELD)
// XML文件中的根标识
@XmlRootElement(name = "Request")
// 控制JAXB 绑定类中属性和字段的排序
@XmlType(propOrder = {
        "Head",
        "Body",
})
public class CriticalValueAdd {

    public static void main(String[] args) {
        CriticalValueAdd ca = new CriticalValueAdd();
        CriticalValueAddBody cab = new CriticalValueAddBody();
        ReqHead rh= new ReqHead();
        ca.Head=rh;
        ca.Body=cab;
        System.out.println(ca.toString());
    }

    private ReqHead Head;
    private CriticalValueAddBody Body;

    public ReqHead getHead() {
        return Head;
    }

    public void setHead(ReqHead head) {
        Head = head;
    }

    public CriticalValueAddBody getBody() {
        return Body;
    }

    public void setBody(CriticalValueAddBody body) {
        Body = body;
    }

    @Override
    public String toString() {
        return "CriticalValueAdd{" +
                "Head=" + Head +
                ", Body=" + Body +
                '}';
    }
}
