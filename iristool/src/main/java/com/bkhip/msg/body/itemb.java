package com.bkhip.msg.body;

/**
 * @author biver
 * @version 1.0
 * @description
 * @date 2023/8/22 10:44:58
 */
public class itemb {
    private String code = "";
    private String name = "";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "item{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
