package com.bkhip.msg.body;

import java.util.List;

/**
 * @author biver
 * @version 1.0
 * @description
 * @date 2023/8/22 10:27:37
 */
public class item {
    private List<itemb> item;

    public List<itemb> getItem() {
        return item;
    }

    public void setItem(List<itemb> item) {
        this.item = item;
    }
}
