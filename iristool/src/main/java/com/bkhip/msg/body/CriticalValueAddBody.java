package com.bkhip.msg.body;

import com.bkhip.msg.ReqHead;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.lang.reflect.Field;
import java.util.List;


/**
 * @author biver
 * @version 1.0
 * @description CriticalValueAdd的body
 * @date 2023/8/17 17:21:44
 */
@XmlAccessorType(XmlAccessType.FIELD)
// XML文件中的根标识
@XmlRootElement(name = "Request")
// 控制JAXB 绑定类中属性和字段的排序
@XmlType(propOrder = {
        "code",
        "name",
        "time",
        "items",
})
public class CriticalValueAddBody {


public static void main(String[] args) {
        CriticalValueAddBody cab = new CriticalValueAddBody();
        System.out.println(cab.toString());
    }

    private String code = "shaihid";
    private String name = "shaihid";
    private String time = "shaihid";
    private item items;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public item getItems() {
        return items;
    }

    public void setItems(item items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "CriticalValueAddBody{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", items=" + items +
                '}';
    }
}
