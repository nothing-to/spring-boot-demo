package com.bkhip.msg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author biver
 * @version 1.0
 * @description 请求的头部
 * @date 2023/8/17 17:05:08
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Computer")
@XmlType(propOrder = {
        "MessageID",
        "CreationTime",
        "SenderID",
        "ReceiverID",
        "IfReceipt"
})
public class ReqHead {

    public static void main(String[] args) {
        ReqHead rh = new ReqHead();
        System.out.println(rh.toString());
    }

    private String MessageID = UUID.randomUUID().toString().replaceAll("-", "");
    ;
    private String CreationTime = setCT();
    private String SenderID = "BKIP";
    private String ReceiverID = "BKIP";
    private String IfReceipt = "N";

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }

    public static String setCT() {
        Date now = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
        return f.format(now);
    }

    public String getCreationTime() {
        return CreationTime;
    }

    public void setCreationTime(String creationTime) {
        CreationTime = creationTime;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getIfReceipt() {
        return IfReceipt;
    }

    public void setIfReceipt(String ifReceipt) {
        IfReceipt = ifReceipt;
    }

    @Override
    public String toString() {
        return "ReqHead{" +
                "MessageID='" + MessageID + '\'' +
                ", CreationTime='" + CreationTime + '\'' +
                ", SenderID='" + SenderID + '\'' +
                ", ReceiverID='" + ReceiverID + '\'' +
                ", IfReceipt='" + IfReceipt + '\'' +
                '}';
    }
}
