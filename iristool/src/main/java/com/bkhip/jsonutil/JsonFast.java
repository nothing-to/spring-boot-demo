package com.bkhip.jsonutil;

import cn.hutool.core.exceptions.UtilException;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.bkhip.msg.body.CriticalValueAddBody;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author biver
 * @version 1.0
 * @date 2023/8/17 14:35
 * 将json字符串按照输入顺序对象化示例
 */
public class JsonFast {

    public static void main(String[] args) {
        String content = "{\"text1\":\"123\",\"text2\":\"456\",\"text3\":\"789\"}";
        Map paraMap = JSON.parseObject(content);
        System.out.println(paraMap.toString());
        paraMap = JSON.parseObject(content, LinkedHashMap.class, Feature.OrderedField);
        System.out.println(paraMap.toString());
        paraMap = JSON.parseObject(content,Feature.OrderedField);
        System.out.println(paraMap.toString());

        CriticalValueAddBody cva = new CriticalValueAddBody();
        HashMap<String, Object> hashMap = JSONUtil.toBean(JSONUtil.parseObj(cva), HashMap.class);
        System.out.println(hashMap.toString());







    }




}


