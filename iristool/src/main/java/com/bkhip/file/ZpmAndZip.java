package com.bkhip.file;

import cn.hutool.core.exceptions.UtilException;
import cn.hutool.core.util.ZipUtil;

/**
 * @author biver
 * @version 1.0
 * @date 2023/8/17 14:41
 * 文件压缩和解压  https://doc.hutool.cn/pages/ZipUtil/
 */
public class ZpmAndZip {

   /**
    * 把指定文件夹压缩到指定位置
    * @param source
    * @param target
    * @return boolean
    * @author wz
    * @date 2023/8/17 15:02:09
    */
    public boolean zpm2zip(String source,String target){
        try {
            ZipUtil.zip(source, target);
        }catch (UtilException exception){
            System.out.println("压缩失败:"+exception.getMessage());
            return false;
        }
        return true;
    }

    /***
     * 把压缩包解压到指定位置
     * @param source
     * @param target
     * @return boolean
     * @author wz
     * @date 2023/8/17 15:11:43
     */
    public boolean zip2zpm(String source,String target){
        try {
            ZipUtil.zip(source, target);
        }catch (UtilException exception){
            System.out.println("解压失败:"+exception.getMessage());
            return false;
        }
        return true;
    }


}
