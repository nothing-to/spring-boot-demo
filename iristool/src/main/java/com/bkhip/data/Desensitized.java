package com.bkhip.data;

import cn.hutool.core.util.DesensitizedUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author biver
 * @version 1.0
 * @description 数据脱敏
 * @date 2023/8/17 15:17:11
 */
public class Desensitized
{




    public static void main(String[] args) {
        System.out.println(desensitized("420324199806231016", "ID_CARD"));
    }
    /***
     * 字符串数字脱敏
     * @param str
     * @param desensitizedType
     * @return java.lang.String
     * @author wz
     * @date 2023/8/17 15:43:04
     */
    public static String desensitized(CharSequence str, String desensitizedType) {
        if (StrUtil.isBlank(str)) {
            return "";
        } else {
            String newStr = String.valueOf(str);
            switch(desensitizedType) {
                case "USER_ID":
                    newStr = String.valueOf(userId());
                    break;
                case "CHINESE_NAME":
                    newStr = DesensitizedUtil.chineseName(String.valueOf(str));
                    break;
                case "ID_CARD":
                    newStr = DesensitizedUtil.idCardNum(String.valueOf(str), 1, 2);
                    break;
                case "FIXED_PHONE":
                    newStr = DesensitizedUtil.fixedPhone(String.valueOf(str));
                    break;
                case "MOBILE_PHONE":
                    newStr = DesensitizedUtil.mobilePhone(String.valueOf(str));
                    break;
                case "ADDRESS":
                    newStr = DesensitizedUtil.address(String.valueOf(str), 8);
                    break;
                case "EMAIL":
                    newStr = DesensitizedUtil.email(String.valueOf(str));
                    break;
                case "PASSWORD":
                    newStr = DesensitizedUtil.password(String.valueOf(str));
                    break;
                case "CAR_LICENSE":
                    newStr = DesensitizedUtil.carLicense(String.valueOf(str));
                    break;
                case "BANK_CARD":
                    newStr = DesensitizedUtil.bankCard(String.valueOf(str));
                    break;
                case "IPV4":
                    newStr = DesensitizedUtil.ipv4(String.valueOf(str));
                    break;
                case "IPV6":
                    newStr = DesensitizedUtil.ipv6(String.valueOf(str));
                    break;
                case "FIRST_MASK":
                    newStr = DesensitizedUtil.firstMask(String.valueOf(str));
            }

            return newStr;
        }
    }

    public static Long userId() {
        return 0L;
    }

}
