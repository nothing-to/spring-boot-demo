package com.bkhip.log;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import cn.hutool.log.level.Level;

/**
 * @author biver
 * @version 1.0
 * @date 2023/8/17 14:33
 * log4j调用示例
 */
public class LogDemo {

    public static void main(String[] args) {
        Log log = LogFactory.get();
        try {
            log.debug("This is {} log", Level.DEBUG);
            log.info("This is {} log", Level.INFO);
            log.warn("This is {} log", Level.WARN);
            Exception e = new Exception("test Exception");
            log.error(e, "This is {} log", Level.ERROR);
            log.info("This is {} log", Level.INFO);

        } catch (Exception ex) {
            System.out.println("存在错误" + ex.getMessage());


        }


    }
}
