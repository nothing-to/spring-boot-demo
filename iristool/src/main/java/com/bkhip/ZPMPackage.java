package com.bkhip;

import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.exceptions.UtilException;
import cn.hutool.core.lang.Console;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.http.webservice.SoapClient;

import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.bkhip.msg.CriticalValueAdd;
import com.bkhip.msg.ReqHead;
import com.bkhip.msg.body.CriticalValueAddBody;

import java.lang.reflect.Field;
import java.util.*;

import com.bkhip.msg.body.item;
import com.bkhip.msg.body.itemb;
import com.bkhip.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ZPMPackage {

    public boolean zpmtozip(){
        try {
            ZipUtil.zip("C:/InterSystems/IRISHealth/Mgr/Temp/Backupfiles/zpm",
                    "C:/zpm/iris.zip");
        }catch (UtilException exception){
            System.out.println("压缩失败:"+exception.getMessage());
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        write();





    }

    /**
     * 对象转Map
     * @param object
     * @return
     */
    public static Map beanToMap(Object object) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName(), field.get(object));
        }
        return map;
    }

    /***
     * @description TODO
     * 管控平台标准消息
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author wz
     * @date 2023/8/17 17:02:19
     */
    public static Map<String, Object> write(){
        Log log = LogFactory.get();
        String result = "";
        String requestId = UUID.randomUUID().toString();
        try{

            String action = "regularUpdate";
            //拼接strXML
            CriticalValueAdd ca = new CriticalValueAdd();
            CriticalValueAddBody cab = new CriticalValueAddBody();
            ReqHead rh = new ReqHead();
            List<itemb> itemList = new ArrayList<>();
            itemb itemb = new itemb();
            itemb.setCode("1");
            itemb.setName("男");
            itemList.add(itemb);
            itemb.setCode("2");
            itemb.setName("女");
            itemList.add(itemb);
            item item = new item();
            item.setItem(itemList);
            cab.setItems(item);
            ca.setHead(rh);
            ca.setBody(cab);

           /* HashMap<String, Object> hashMap = JSONUtil.toBean(JSONUtil.parseObj(ca), HashMap.class);
            System.out.println(hashMap.toString());
            String message = XmlUtil.mapToXmlStr(beanToMap(ca));;
            System.out.println(message);*/
            String message = XMLUtil.convertToXml(ca);
            System.out.println(message);
            String paramStr = "<![CDATA["+ message+"]]>";
            //拼接外层入参
            Map<String,Object> params = new HashMap<>();
            params.put("action",action);
            params.put("message",paramStr);
            //新建客户端
            String url = "";
            SoapClient client = SoapClient.create("http://192.168.90.100:52/bklinik/ESB.SoapService.cls")
                    .header("SOAPAction","")
                    // 设置要请求的方法，此接口方法前缀为web，传入对应的命名空间
                    .setMethod("bkg:Send", "http://www.bkgtsoft.com")
                    .setParams(params,true);

            log.info("[请求ID:{}]CSDN测试方法字符串接口:{},入参:{}",requestId,url,client.getMsgStr(true));
            // 发送请求，参数true表示返回一个格式化后的XML内容7
            // 返回内容为XML字符串，可以配合XmlUtil解析这个响应
            result = client.send(false);
            log.info("[请求ID:{}]CSDN测试方法字符串出参:{}",requestId,result);
            if(result.contains("<![CDATA[")){
                result = result.substring(result.indexOf("<![CDATA[")+9,result.indexOf("]]>"));
            }else{
                result = result.substring(result.indexOf("Result>")+7,result.indexOf("}</")+1);
            }
            Map<String, Object> resultMap = XmlUtil.xmlToMap(result);
            return resultMap;
        }catch (Exception ex){
            log.error("[请求ID:{}]CSDN测试方法字符串出现异常出参:{},{}",requestId,result,ex);
//            throw ex;
        }
        return null;
    }

}
