package com.bkhip.util;

//import com.bk.hip.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.util.IOUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * <p>
 * 文件处理
 * </p>
 * @author wanyifan
 * @since 2022-11-7 10:37
 */
@Slf4j
public class FileUtils {

    /**
     * 输出指定文件的byte数组
     *
     * @param filePath 文件路径
     * @param os       输出流
     */
    public static void writeBytes(String filePath, OutputStream os) throws IOException {
        FileInputStream fis = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException(filePath);
            }
            fis = new FileInputStream(file);
            byte[] b = new byte[1024];
            int length;
            while ((length = fis.read(b)) > 0) {
                os.write(b, 0, length);
            }
        } finally {
            os.close();
            assert fis != null;
            fis.close();

        }
    }




    /**
     * 删除文件
     *
     * @param filePath 文件
     * @return boolean
     */
    public static boolean deleteFile(String filePath) {
        boolean flag = false;
        File file = new File(filePath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            flag = file.delete();
        }
        return flag;
    }



    /**
     * 下载文件名重新编码
     *
     * @param response     响应对象
     * @param realFileName 真实文件名
     */
    public static void setAttachmentResponseHeader(HttpServletResponse response, String realFileName) throws UnsupportedEncodingException {
        String percentEncodedFileName = percentEncode(realFileName);

        StringBuilder contentDispositionValue = new StringBuilder();
        contentDispositionValue.append("attachment; filename=")
                .append(percentEncodedFileName)
                .append(";")
                .append("filename*=")
                .append("utf-8''")
                .append(percentEncodedFileName);

        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition,download-filename");
        response.setHeader("Content-disposition", contentDispositionValue.toString());
        response.setHeader("download-filename", percentEncodedFileName);
    }

    /**
     * 百分号编码工具方法
     *
     * @param s 需要百分号编码的字符串
     * @return 百分号编码后的字符串
     */
    public static String percentEncode(String s) throws UnsupportedEncodingException {
        String encode = URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
        return encode.replaceAll("\\+", "%20");
    }

    /**
     * 获取图像后缀
     *
     * @param photoByte 图像数据
     * @return 后缀名
     */
    public static String getFileExtendName(byte[] photoByte) {
        String strFileExtendName = "jpg";
        if ((photoByte[0] == 71) && (photoByte[1] == 73) && (photoByte[2] == 70) && (photoByte[3] == 56)
                && ((photoByte[4] == 55) || (photoByte[4] == 57)) && (photoByte[5] == 97)) {
            strFileExtendName = "gif";
        } else if ((photoByte[6] == 74) && (photoByte[7] == 70) && (photoByte[8] == 73) && (photoByte[9] == 70)) {
            strFileExtendName = "jpg";
        } else if ((photoByte[0] == 66) && (photoByte[1] == 77)) {
            strFileExtendName = "bmp";
        } else if ((photoByte[1] == 80) && (photoByte[2] == 78) && (photoByte[3] == 71)) {
            strFileExtendName = "png";
        }
        return strFileExtendName;
    }

    /*
     * 描述:不用写temp路径,直接传入输入流集合和sourceNames名称数组,注意名称和流需要对应
     * 参数:sourceInputStreamList 流集合; zipName zip名称; sourceNames 存进zip中的文件名称;
     * */
    public static void createZipByInputStream(List<InputStream> sourceInputStreamList, String zipName, String[] sourceNames, HttpServletResponse response) {
        BufferedInputStream bis = null;
        OutputStream fos;
        ZipOutputStream zos = null;
        try {
            response.setCharacterEncoding("UTF-8");
            zipName = URLEncoder.encode(zipName, "UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + zipName);
            response.setContentType("application/zip");// 定义输出类型*/
            if (sourceInputStreamList.size() < 1) {
                log.error("没有传递过来输入流文件");
            } else {
                fos = response.getOutputStream();
                zos = new ZipOutputStream(new BufferedOutputStream(fos));
                for (int i = 0; i < sourceInputStreamList.size(); i++) {
                    //创建ZIP并且将文件放入ZIP中
                    ZipEntry zipEntry = new ZipEntry(sourceNames[i]);
                    zos.putNextEntry(zipEntry);
                    IOUtils.copy(sourceInputStreamList.get(i), zos);
                }
                zos.flush();
            }
        } catch (FileNotFoundException e) {
            log.error("没有发现文件");
//            throw new BaseException("没有发现文件");
        } catch (IOException e) {
            log.error("使用IO流出现异常");
//            throw new BaseException("使用IO流出现异常");
        } finally {
            closeAll(bis, zos);
        }
    }

    public static void closeAll(AutoCloseable... closeables) {
        for (AutoCloseable c : closeables) {
            if (c != null) {
                try {
                    c.close();
                } catch (Exception e) {
                    log.error("关闭出错", e);
                }
            }

        }
    }
}
