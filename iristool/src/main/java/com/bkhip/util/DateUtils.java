package com.bkhip.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 日期工具类
 * @author wanyifan
 * @date 2023-3-9 9:26
 * @Version 1.0
 * @desc
 */
public class DateUtils {

    /**
     * 获取最近day天日期
     * @param days 最近天数
     * @return
     */
    public static List<String> getDaysBetween(int days){ //最近几天日期
        List<String> dayss = new ArrayList<>();
        Calendar start = Calendar.getInstance();
        start.setTime(getDateAdd(days-1));
        long startTime = start.getTimeInMillis();
        Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        long endTime = end.getTimeInMillis();
        long oneDay = 1000 * 60 * 60 * 24L;
        long time = startTime;
        while (time <= endTime) {
            Date d = new Date(time);
            DateFormat df = new SimpleDateFormat("MM-dd");
            System.out.println(df.format(d));
            dayss.add(df.format(d));
            time += oneDay;
        }
        return dayss;
    }

    /**
     * 不能超过30天
     * @param days
     * @return
     */
    public static Date getDateAdd(int days){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -days);
        return c.getTime();
    }

    public static LocalDateTime getLocalDateTimeByDate(Date date){
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        return instant.atZone(zoneId).toLocalDateTime();
    }

    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(sdf.format(new Date()));
        Date date = calculationDate(new Date(),-90);
        System.out.println(sdf.format(date));
        System.out.println(getLocalDateTimeByDate(date));
    }

    //传入指定的日期beginDay和时间间隔days，往前为负数，往后为正数
    public static Date calculationDate(Date beginDay, long days){
        //获取指定日期的时间戳
        long beginTime= beginDay.getTime();
        //计算时间间隔的时间戳
        long intervalTime = days*24*60*60*1000;
        //用指定日期时间戳加上时间间隔得到所求的日期
        long lastTime = beginTime + intervalTime;
        //将所求日期的时间戳转为日期并返回
        return new Date(lastTime);
    }
}
