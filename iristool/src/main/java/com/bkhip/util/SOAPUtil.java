package com.bkhip.util;

import cn.hutool.core.util.XmlUtil;
import cn.hutool.http.webservice.SoapClient;

import com.bkhip.msg.CriticalValueAdd;
import com.bkhip.msg.ReqHead;
import com.bkhip.msg.body.CriticalValueAddBody;
import com.bkhip.msg.body.item;
import com.bkhip.msg.body.itemb;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author biver
 * @version 1.0
 * @description webservice工具类
 * @date 2023/8/22 11:02:12
 */
public class SOAPUtil {

    private static Logger log = LoggerFactory.getLogger(SOAPUtil.class);

    public static void main(String[] args) {
//        String action = "regularUpdate";
//        //拼接strXML
//        CriticalValueAdd ca = new CriticalValueAdd();
//        CriticalValueAddBody cab = new CriticalValueAddBody();
//        ReqHead rh = new ReqHead();
//        List<itemb> itemList = new ArrayList<>();
//        itemb itemb = new itemb();
//        itemb.setCode("1");
//        itemb.setName("男");
//        itemList.add(itemb);
//        itemb.setCode("2");
//        itemb.setName("女");
//        itemList.add(itemb);
//        item item = new item();
//        item.setItem(itemList);
//        cab.setItems(item);
//        ca.setHead(rh);
//        ca.setBody(cab);
//        System.out.println(request("bkg:Send",action, ca).toString());



        StringBuilder params = new StringBuilder("<![CDATA[<request>");
        params.append("<type>0</type></request>]]>");
        request("DictInfoAdd",params.toString());
    }

    /***
     * @description BKlinik 定义的方式
     * @param action
     * @param obj
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author wz
     * @date 2023/8/22 11:07:25
     */
    public static Map<String, Object> request(String method,String action,Object obj){

        String result = "";
        String requestId = UUID.randomUUID().toString();
        try{
            String message = XMLUtil.convertToXml(obj);
            System.out.println(message);
            String paramStr = "<![CDATA["+ message +"]]>";
            //拼接外层入参
            Map<String,Object> params = new HashMap<>();
            params.put("action",action);
            params.put("message",paramStr);
            //新建客户端
            String url = "";
            SoapClient client = SoapClient.create("http://192.168.90.100:52/bklinik/ESB.SoapService.cls")
                    .header("SOAPAction","")
                    // 设置要请求的方法，此接口方法前缀为web，传入对应的命名空间
                    .setMethod(method, "http://www.bkgtsoft.com")
                    .setParams(params,true);

            log.info("[请求ID:{}]CSDN测试方法字符串接口:{},入参:{}",requestId,url,client.getMsgStr(true));
            // 发送请求，参数true表示返回一个格式化后的XML内容7
            // 返回内容为XML字符串，可以配合XmlUtil解析这个响应
            result = client.send(false);
            log.info("[请求ID:{}]CSDN测试方法字符串出参:{}",requestId,result);
            if(result.contains("<![CDATA[")){
                result = result.substring(result.indexOf("<![CDATA[")+9,result.indexOf("]]>"));
            }else{
                result = result.substring(result.indexOf("Result>")+7,result.indexOf("}</")+1);
            }
            Map<String, Object> resultMap = XmlUtil.xmlToMap(result);
            return resultMap;
        }catch (Exception ex){
            log.error("[请求ID:{}]CSDN测试方法字符串出现异常出参:{},{}",requestId,result,ex);
//            throw ex;
        }
        return null;
    }
    /**
     * @description: 请求方法
     * @param: method 请求方法
     * @param: params 入参字符串
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author wz
     * @date: 2022/10/24 14:23
     */
    private static Map<String, Object> request(String method, String params){
        String result = "";
        String requestId = UUID.randomUUID().toString();
        try{
            String url = "http://127.0.0.1:8080/url";
            SoapClient client = SoapClient.create(url)
                    .header("SOAPAction","")
                    // 设置要请求的方法，此接口方法前缀为web，传入对应的命名空间
                    .setMethod("bkg:" + method, "http://bkgtsoft.com")
                    .setParam("pInput",params,false);//此处写true，会自动填写命名空间

            log.info("[请求ID:{}]CSDN测试方法字符串接口:{},入参:{}",requestId,url,client.getMsgStr(true));
            // 发送请求，参数true表示返回一个格式化后的XML内容
            // 返回内容为XML字符串，可以配合XmlUtil解析这个响应
            result = client.send(false);
            log.info("[请求ID:{}]CSDN测试方法字符串出参:{}",requestId,result);
            if(result.contains("<![CDATA[")){
                result = result.substring(result.indexOf("<![CDATA[")+9,result.indexOf("]]>"));
            }else{
                result = result.substring(result.indexOf("Result>")+7,result.indexOf("}</")+1);
            }
            Map<String, Object> resultMap = XmlUtil.xmlToMap(result);
            return resultMap;
        }catch (Exception ex){
            log.error("[请求ID:{}]CSDN测试方法字符串出现异常,入参:{},出参:{},{}",requestId,params,result,ex);
            throw ex;
        }
    }
}
