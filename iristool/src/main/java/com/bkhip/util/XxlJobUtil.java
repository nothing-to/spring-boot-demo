//package com.bkhip.util;
//
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.lang.Assert;
//import cn.hutool.core.util.StrUtil;
//import cn.hutool.http.HttpException;
//import cn.hutool.http.HttpGlobalConfig;
//import cn.hutool.http.HttpRequest;
//import cn.hutool.http.HttpUtil;
//import cn.hutool.json.JSONArray;
//import cn.hutool.json.JSONObject;
//import cn.hutool.json.JSONUtil;
//import com.bk.hip.esbmcp.constant.ScheduleTypeConstant;
//import com.bk.hip.esbmcp.model.dto.XxlJobDTO;
//import com.bk.hip.esbmcp.model.dto.XxlJobGroupDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.annotation.PostConstruct;
//import java.net.CookieStore;
//import java.net.HttpCookie;
//import java.util.*;
//
//@Slf4j
//@Component
//public class XxlJobUtil {
//
//    @Value("${hip.job.adminAddresses:http://localhost:19002/xxl-job-admin/}")
//    private String adminAddresses;
//
//    private static final int SUCCESS_CODE = 200;
//
//    private static final int FAIL_CODE = 500;
//
//    private static final String LOGIN_IDENTITY_KEY = "XXL_JOB_LOGIN_IDENTITY";
//
//    private static HttpCookie XXL_JOB_COOKIE = null;
//
//
//    /**
//     * xxljob登录
//     */
//    @PostConstruct
//    public boolean loginXxlJob() {
//        log.info("xxlJob服务初始化...");
//        CookieStore cookieStore = HttpRequest.getCookieManager().getCookieStore();
//        cookieStore.removeAll();
//
//        String path = adminAddresses + "login";
//        Map<String, Object> map = new HashMap<>();
//        map.put("userName", "admin");
//        map.put("password", "123456");
//        map.put("ifRemember", "on");
//        try {
//            HttpUtil.post(path, map);
//            XXL_JOB_COOKIE = cookieStore.getCookies()
//                    .stream()
//                    .filter(cookie -> StrUtil.equals(LOGIN_IDENTITY_KEY, cookie.getName()))
//                    .findFirst()
//                    .orElseThrow(RuntimeException::new);
//            log.info("xxlJob初始化完成, 当前cookie: {}", XXL_JOB_COOKIE);
//        }catch (Exception e) {
//            log.error("xxlJob初始化失败, 请排查原因...");
//        }
//        return true;
//    }
//
//    /**
//     * 分页条件查询
//     */
//    public List<XxlJobDTO> pageList(@RequestParam(required = false, defaultValue = "0") int start,
//                                     @RequestParam(required = false, defaultValue = "10") int length,
//                                     int jobGroup,
//                                     int triggerStatus,
//                                     String jobDesc,
//                                     String executorHandler,
//                                     String author) {
//        String path =  adminAddresses +"jobinfo/pageList";
//        Map<String, Object> map = new HashMap<>();
//        map.put("start", start);
//        map.put("length", length);
//        map.put("jobGroup", jobGroup);
//        map.put("triggerStatus", triggerStatus);
//        map.put("jobDesc", jobDesc);
//        map.put("executorHandler", executorHandler);
//        map.put("author", author);
//
//        String json;
//        try {
//            HttpRequest request = HttpUtil.createPost(path);
//            Map<String, String> header = new HashMap<>(1);
//            header.put("Cookie", StrUtil.format("{}={}", LOGIN_IDENTITY_KEY, XXL_JOB_COOKIE.getValue()));
//            json = request.addHeaders(header).timeout(HttpGlobalConfig.getTimeout()).form(map).execute().body();
//            JSONObject jsonObject = JSONUtil.parseObj(json);
//            JSONArray jsonArray = jsonObject.getJSONArray("data");
//            if (jsonArray == null) {
//                return Collections.emptyList();
//            }
//            log.info("xxlJob查询结果：{}", jsonArray);
//            return jsonArray.toList(XxlJobDTO.class);
//        } catch (HttpException e) {
//            log.info("xxlJob分页查询出现异常，异常原因：{}", e.getMessage());
//        }
//        return Collections.emptyList();
//    }
//
//    /**
//     * 新增任务
//     */
//    public String addXxlJob(XxlJobDTO dto) {
//        Assert.isNull(dto.getId());
//        String path =  adminAddresses +"jobinfo/add";
//        return callXxlJobId(path, BeanUtil.beanToMap(dto));
//    }
//
//    /**
//     * 更新任务
//     */
//    public void updateXxlJob(XxlJobDTO dto) {
//        Assert.notNull(dto.getId());
//        String path = adminAddresses +"jobinfo/update";
//        callSucceeded(path, BeanUtil.beanToMap(dto));
//    }
//
//    /**
//     * 开启任务
//     */
//    public boolean startXxlJob(String id) {
//        String path = adminAddresses +"jobinfo/start";
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", Integer.parseInt(id));
//        return callSucceeded(path, map);
//    }
//
//    /**
//     * 停止任务
//     */
//    public boolean stopXxlJob(String id) {
//        String path =  adminAddresses +"jobinfo/stop";
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", Integer.parseInt(id));
//        return callSucceeded(path, map);
//    }
//
//    /**
//     * 删除任务
//     */
//    public boolean removeXxlJob(String id) {
//        String path =  adminAddresses +"jobinfo/remove";
//        Map<String, Object> map = new HashMap<>();
//        map.put("id", Integer.parseInt(id));
//        return callSucceeded(path, map);
//    }
//
//    /**
//     * 新增执行器
//     */
//    public String addXxlJobGroup(XxlJobGroupDTO dto) {
//        //Assert.isNull(dto.getId());
//        String path =  adminAddresses +"jobgroup/save";
//        return postXxlJob(path,  BeanUtil.beanToMap(dto));
//    }
//
//    /**
//     * 获取分组ID
//     */
//    public int getXxlJobGroupId(String appName) {
//        Assert.notBlank(appName, () -> new RuntimeException("appName不能为空"));
//        String path =  adminAddresses +"jobgroup/loadByAppName";
//        Map<String, Object> map = new HashMap<>();
//        map.put("appName", appName);
//        String json = postXxlJob(path, map);
//        Object xxlJobGroup = JSONUtil.parseObj(json).get("content");
//        // TODO: 2023/3/6 500判定
//        Assert.notNull(xxlJobGroup, () -> new RuntimeException("xxlJobGroup不存在"));
//        Object id = JSONUtil.parseObj(xxlJobGroup).get("id");
//        return (int) Optional.ofNullable(id).orElseThrow(RuntimeException::new);
//    }
//
//    private boolean callSucceeded(String path, Map<String, Object> formMap) {
//        String json = postXxlJob(path, formMap);
//        int respCode = (int) JSONUtil.parseObj(json).get("code");
//        Assert.isTrue(SUCCESS_CODE == respCode);
//        return true;
//    }
//
//    private String callXxlJobId(String path, Map<String, Object> formMap) {
//        String json = postXxlJob(path, formMap);
//        int respCode = (int) JSONUtil.parseObj(json).get("code");
//        Assert.isTrue(SUCCESS_CODE == respCode);
//        Object xxlJobId = JSONUtil.parseObj(json).get("content");
//        Assert.notNull(xxlJobId);
//        return xxlJobId.toString();
//    }
//
//    private String postXxlJob(String path, Map<String, Object> formMap) {
//        if (XXL_JOB_COOKIE == null) {
//            this.loginXxlJob();
//        }
//        HttpRequest request = HttpUtil.createPost(path);
//        Map<String, String> header = new HashMap<>(1);
//        header.put("Cookie", StrUtil.format("{}={}", LOGIN_IDENTITY_KEY, XXL_JOB_COOKIE.getValue()));
//        String json = request.addHeaders(header).timeout(HttpGlobalConfig.getTimeout()).form(formMap).execute().body();
//
//        // 如果返回状态值不为200，则重新登录一次获取最新的cookie，并再次发送请求
//        int respCode = (int) JSONUtil.parseObj(json).get("code");
//        if (SUCCESS_CODE != respCode) {
//            this.loginXxlJob();
//            header.put("Cookie", StrUtil.format("{}={}", LOGIN_IDENTITY_KEY, XXL_JOB_COOKIE.getValue()));
//            json = request.addHeaders(header).timeout(HttpGlobalConfig.getTimeout()).form(formMap).execute().body();
//        }
//        return json;
//    }
//
//    public XxlJobDTO buildXxlJobDTO() {
//        XxlJobDTO xxlJobDTO = new XxlJobDTO();
//        xxlJobDTO.setScheduleType(ScheduleTypeConstant.NONE);
//        xxlJobDTO.setMisfireStrategy("DO_NOTHING");
//        xxlJobDTO.setExecutorRouteStrategy("ROUND");
//        xxlJobDTO.setExecutorParam("");
//        xxlJobDTO.setExecutorBlockStrategy("DISCARD_LATER");
//        xxlJobDTO.setExecutorTimeout(0);
//        xxlJobDTO.setExecutorFailRetryCount(0);
//        xxlJobDTO.setGlueType("BEAN");
//        xxlJobDTO.setGlueRemark("GLUE代码初始化");
//        xxlJobDTO.setTriggerStatus(0);
//        xxlJobDTO.setTriggerLastTime(0L);
//        xxlJobDTO.setTriggerNextTime(0L);
//        return xxlJobDTO;
//    }
//}
