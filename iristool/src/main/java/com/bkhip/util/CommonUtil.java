package com.bkhip.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.io.StringReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wanyifan
 * @date 2022-11-2 11:38
 * @Version 1.0
 * @desc
 */
public class CommonUtil {



    /**
     * 服务标识后缀长度
     */
    public static int LENGTH = 8;


    /**
     * 字符串转毫秒
     *
     * @throws ParseException
     */
    public static long strToMs(String dateTime) throws ParseException {
        long result = 0L;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(dateTime));
            result =  calendar.getTimeInMillis();
        }catch (Exception e){
            return result;
        }
        return result;
    }

    /**
     * 判断字符串中是否包含中文
     *
     * @param str 待校验字符串
     * @return 是否为中文
     * @warn 不能校验是否为中文标点符号
     */
    public static boolean isContainsChinese(String str) {
        if (str == null) {
            return false;
        }
        String regex = "[\u4e00-\u9fa5]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 是否只包含英文和数字且以字母开头
     *
     * @param str
     * @return
     */
    public static boolean isCaseNum(String str) {
        if (str == null) {
            return false;
        }
        String regex = "^[a-zA-Z][a-zA-Z0-9]*$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 别名是否只包含英文和数字且以字母开头
     *
     * @param str
     * @return
     */
    public static boolean isAliasCaseNum(String str) {
        if (str == null) {
            return false;
        }
        String regex = "^[a-zA-Z][a-zA-Z0-9_]*$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.find();
    }
    public static void main(String[] args) throws ParseException {
        System.out.println(CommonUtil.strToMs(null));
        System.out.println(isAliasCaseNum("a_A"));
    }
    /**
     * 同步HttpPost请求发送消息请求
     *
     * @param URL 监控服务器接口地址
     * @return
     */
    public static String doPost(String URL) {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
        // 创建Post请求
        HttpPost httpPost = new HttpPost(URL);
        // 设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(3000)
                .setConnectTimeout(3000).build();
        httpPost.setConfig(requestConfig);
        try {
            // 执行请求获取返回报文
            CloseableHttpResponse response = closeableHttpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                // 打印响应内容
                return EntityUtils.toString(httpEntity, "UTF-8");

            }
            // 释放资源
            closeableHttpClient.close();

        } catch (ClientProtocolException e) {
            e.printStackTrace();

        } catch (ConnectException e) {
            return "no";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param path
     * @return
     */
    public static String requestESB(String path) {
        String addr = "http:127.0.0.1:52773";
        return HttpRequest.get(addr + path)
                .basicAuth("BKlinik", "BKlinik")
                .contentType("application/json")
                .execute().body();
    }

    /**
     * 判断url是否可用
     *
     * @param url
     * @return
     */
    public static boolean isConnect(String url) {
        try {
            URL urlAdd = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlAdd.openConnection();
            int state = conn.getResponseCode();
            if (state == 200) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 根据wsdl地址获取里面的方法
     *
     * @param wsdlAddr 地址
     * @return 方法集合
     * @throws Exception
     */
    public static List<String> getWsdlMethod(String wsdlAddr) throws Exception {
        List<String> wsdlMehtod = new ArrayList<>();
        SAXReader saxReader = new SAXReader();
        String s3 = HttpUtil.get(wsdlAddr);
        StringReader stringReader = new StringReader(s3);
        Document document = saxReader.read(stringReader);
        Element rootElement = document.getRootElement();
        getNodes(rootElement, wsdlMehtod);
        return wsdlMehtod;
    }

    /**
     * 递归节点并进行方法存储
     *
     * @param node       节点
     * @param wsdlMehtod 集合
     */
    public static void getNodes(Element node, List<String> wsdlMehtod) {
        String name = node.getName();//获取所有operation节点中属性名为name的值,与银行确认
        if ("operation".equalsIgnoreCase(name)) {
            Attribute attribute = node.attribute("name");
            if (ObjectUtil.isNotNull(attribute)) {
                String value = attribute.getValue();
                wsdlMehtod.add(value);
            }
        }
        List<Element> elements = node.elements();
        for (Element e : elements) {//遍历所有一级子节点
            getNodes(e, wsdlMehtod);//递归
        }
    }

}
