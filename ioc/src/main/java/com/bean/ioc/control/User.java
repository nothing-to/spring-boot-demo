package com.bean.ioc.control;

import lombok.Data;

@Data
public class User {

    private int age;
    private String name;

}
