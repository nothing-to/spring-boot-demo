package com.bean.ioc.control;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// 注解的ioc方式 value用于用于SpringIOC调用，可以为任意，和bean的id作用一样
// Scope和配置文件中的Scope一致，这里的意思为创建的实例对象为多实例。
@Component(value = "person")
@Scope(value = "prototype")
@Data
public class Person {

    private int id;
    @Value("未知")
    private String name;
    @Value("暂时未申请")
    private String email;


    public void test() {
        System.out.println("注解方式使用ioc ----- test");
    }
}
