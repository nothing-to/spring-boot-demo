package com.bean.ioc.control;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 创建工厂类 通过反射和工厂模式创建
 */
public class Factory {
    //创建返回User对象的方法
    public static User getUser() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String classValue="com.bean.ioc.control.User";
        //使用反射的方法创建对象
        Class aClass=Class.forName(classValue);
        User user= (User) aClass.newInstance();
        return user;
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        testIOC();
        testIOC1();
        testIOC2();
    }

    public static void testIOC() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        System.out.println("创建对象--传统--------通过反射和工厂模式创建--------------------------");
        System.out.println(Factory.getUser());
    }

    public static void testIOC1(){
        System.out.println("创建对象--ioc---xml方式---------------------");
        ApplicationContext context=new ClassPathXmlApplicationContext("application.xml");
        User user1= (User) context.getBean("user");
        User user2=(User) context.getBean("user");
        System.out.println(user1 +""+ user2); //如果scope为单例，两个对象的地址相同，多例效果则相反
    }

    public static void testIOC2(){
        System.out.println("创建对象--ioc-----注解方式-----------------------");
        ApplicationContext context = new
                ClassPathXmlApplicationContext("application.xml");
        Person person = (Person) context.getBean("person");
        person.test();

        System.out.println(person);
    }
}
