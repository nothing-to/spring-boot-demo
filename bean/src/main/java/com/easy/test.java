package com.easy;

import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class test {


    public static void main(String[] args) {


        //tjcs("aghhhjoak");

        StringPractice();
    }

    /**
     * 已知一个字符串，我需要统计里面每一个字符串出现的次数：
     * 创建一个Map容器，存储字符串和以及字符串中出现的次数，
     * 字符串通过.length()获取长度，通过charAt(i)获取每个字符串去统计
     * 遍历字符串，通过map中的key进行判断，如果存在就+1如果不存在就计数为1。
     */
    public static void tjcs(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            if (map.get(str.charAt(i)) == null || "".equals(map.get(str.charAt(i)))) {
                map.put(str.charAt(i), 1);
            } else {
                map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
            }
        }
        System.out.println(map);
    }

    /**
     * String里面常用方法
     * 替换、截取、取前几位、计数等
     */
    public static void StringPractice() {
        String str = "aghhhjoak";
        System.out.println(str);
        // 替换 - replace
        System.out.println(str.replace('a', 'c'));
        // 截取 - split
        System.out.println(Arrays.asList(str.split("j")));
        // 取前几位 - substring
        System.out.println(str.substring(0, 2));
        // 计数
        System.out.println(str.length());
    }
}
