package com.easy;

/**
 * 常量池 Constant Pool
 * 常量池指的是在编译器被确定，并被保存在已变异的class文件中的一些数据。除了包含代码中所定义的各种
 * 基本类型（int，long）和
 * 对象型（String和数组）的常量值（final）还包含一些以文本形式出现的符号引用，比如：
 * 1、类和接口的全限定名
 * 2、字段的名称和描述符
 * 3、方法的名称和描述符
 * 虚拟机必须为每个被装载的类型维护一个常量池。
 * 常量池就是该类型所用到常量的一个有序集合，包括
 * 直接常量（String、interface、floating point）和对其他类型、字段和方法的符号引用
 * 对于String常量，它的值在常量池中的，而JVM中常量池在内存当中是以表的形式存在的，
 * 对于String类型，有一张固定长度的CONSTANT_String_info表用来存储文字字符串值，
 * 注意：该表只存储文字字符串值，不存储符号引用。
 * 在程序执行的时候，常量池会存储在Method Area（方法区）而不是堆中。
 */
public class ConstantPool {

    public static void main(String[] args) {

        /*
        直接定义：在字符串常量区查找是否存在 "hello" 常量，如果不存在，
        则在字符串常量区开辟一个内存空间，存放 "hello"；
        如果存在，则不另外开辟空间;
        在栈区开辟空间，存放变量名称str2，str2指向字符串常量池 "hello" 的内存地址。*/
        String s1 = "hello";  // 此时已把hello放入常量池中
        String s2 = "hello";

        /*
        解析：字符串编译时候保存class文件中，在 s1 等于 hello 时，已把 hello 放入常量池中，
        而 s2 的 hello 是从常量池中取出的，所以s1和s2地址是一样的，
        所以，不论是地址还是内容都返回true
        */
        System.out.println(s1 == s2);  // 比较地址  true
        System.out.println(s1.equals(s2));  // 比较内容  true
        pAdress(s1);
        pAdress(s2);


        /*
        new的方式：编译程序先在字符串常量池查找，是否存在"hello"常量，
        如果不存在，则在字符串常量池开辟一个内存空间，存放"hello"；
        如果存在，则不另外开辟空间，保证字符串常量区只有一个"hello"，节省空间。
        然后在 堆区，开辟一个空间，存放new出来的String 对象 ，
        并在 栈区 开辟空间，存放 变量名称 str1，str1指向堆区new出来的String对象。
        */
        String ss1 = new String("hello");
        System.out.println(ss1 == s1);
        System.out.println(ss1.equals(s1));

        pAdress(ss1);
        pAdress(s1);
    }


    // 打印String地址
    public static void pAdress(String str) {
        System.out.print("地址：");
        System.out.println(
                String.class.getName()
                        + "@"
                        + Integer.toHexString(System.identityHashCode(str))
        );
    }
}
