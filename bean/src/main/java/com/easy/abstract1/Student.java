package com.easy.abstract1;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//写一个学生类去继承人
@Data
@NoArgsConstructor
public class Student extends Person {

    //定义一个学生自己的属性 总分
    private float fenshu;

    //写一个student自己的构造方法
    public Student(String name, int age, float fenshu) {
        super(name, age);
        this.fenshu = fenshu;
    }

    @Override
    public void say() {
        System.out.println("学生: "
                + super.getAge() + " "
                + super.getName() + " "
                + this.fenshu);
    }
}
