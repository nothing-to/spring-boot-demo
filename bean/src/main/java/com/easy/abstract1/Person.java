package com.easy.abstract1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
abstract class Person {
    //写一些人的属性，姓名，年龄，并进行封装
    private String name;
    private int age;

    //写一个构造方法为属性赋值,构造方法不能有返回值类型
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    // 写一个抽象方法：说话，让子类是复写说话的内容
    public abstract  void say();

    public static void main(String[] args) {
        Student s = new Student("张三",18,78.9f);
        s.say();
        Worker w = new Worker("李四",30,20000);
        w.say();
        //上面的是实例化子类分别去调用子类的方法获得的，下方我通过对象转型来实现
        Person p1 = new Student("王五",14,77f);
        p1.say();
        Person p2 = new Worker("刘六",40,15000);
        p2.say();
    }

}


