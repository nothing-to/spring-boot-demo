package com.easy.abstract1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;

//写一个工人的类去继承人
@Data // get()、 set()、 toString()
@NoArgsConstructor // 无参构造函数
@AllArgsConstructor // 全参构造函数
//@RequiredArgsConstructor(onConstructor = @__(@Lazy)) // 构造器注入
class Worker extends Person {

    //写一个工人自己的属性 工资
    private int gongzi;

    //写一个工人自己的构造方法
    public Worker(String name, int age, int gongzi) {
        super(name, age);
        this.gongzi = gongzi;
    }

    //去实现父类中的say方法
    public void say() {
        System.out.println("工人: "
                + super.getAge() + " "
                + super.getName() +" "
                + this.gongzi);
    }
}
