package com.easy.interface1;

/**
 * 老虎 实现为
 */
public class Tiger extends AbstractAnimal implements LikeAnimal,isProtectedAnimal{

    @Override
    public void eat() {
        //实现接口中的eat()抽象方法
        System.out.println("老虎正在吃饭！");
    }


    @Override
    public void travel() {
        //实现接口中的travel()抽象方法
        System.out.println("老虎喜欢旅行！");
    }

    public String park() {
        //此类额外创建了一个新方法
        return "公园里有很多只老虎！";
    }
}
