package com.easy.interface1;

/**
 * 动物接口
 * 接口（interface），在Java语言中是一个抽象类型，是抽象方法的集合，接口通常使用interface来声明
 * 一个类通过继承的方式通过继承接口的方式，从而来继承并且实现接口中的抽象方法。
 * 接口和类不一样，但是编写接口的方式和类很相似，但是他们数据不同的概念。
 * 类描述对象的属性和方法。
 * 接口则包含类要实现的方法，除非实现接口的是抽象类，否则该类要定义并实现接口中的所有抽象方法。
 * 接口无法被实例化，但是可以被实现。一个实现接口的类，必须实现接口内所描述的所有方法，
 * 否则就必须声明为抽象类
 * 另外，在Java语言中，接口类型可用来声明一个变量，他们可以成为一个空指针，或者被绑定在一个以此接口实现的对象
 */
public interface LikeAnimal extends isAnimal {//声明一个接口

    public void eat();//接口中的两个公共方法

    //travel - 旅行
    public void travel();//等待继承此接口的类来实现

    public static void ab() {
        System.out.println("接口中的静态方法！");
    }
}

/**
 * 动物
 */
class PlayAnimal {
    public static void main(String[] args) {
        Tiger a = new Tiger();
        a.eat();
        a.travel();//抽象方法和默认方法通过实现该接口的类的实例对象来调用
        System.out.println(a.park());

        LikeAnimal.ab();//通过 接口名.方法名 来调用接口中的静态方法
    }
}