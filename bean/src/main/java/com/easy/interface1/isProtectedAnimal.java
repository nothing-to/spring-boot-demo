package com.easy.interface1;

/**
 * 标记接口 - 标记是否是保护动物
 */
public interface isProtectedAnimal {

}

class Main {
    public static void main(String[] args){
        Dog dog = new Dog();
        Tiger tiger = new Tiger();

        dog.eat();
        tiger.eat();

        System.out.print("狗 ");
        dog.get(1);
        System.out.print("老虎 ");
        tiger.get(1);

        Rule rule = new Rule();
        System.out.print("尺子 ");
        rule.get(1);

    }
}
