package com.easy.interface1;

/**
 * 狗
 */
public class Dog extends AbstractAnimal implements LikeAnimal  {


    @Override
    public void eat() {
        System.out.println("小狗正在吃狗粮");
    }

    @Override
    public void travel() {
        System.out.println("小狗不喜欢旅行，需要看家");
    }
}

