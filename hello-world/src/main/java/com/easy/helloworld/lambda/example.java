package com.easy.helloworld.lambda;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author biver
 * @version 1.0
 * @description lambda表达式的十个常用方式
 * @date 2023/8/27 14:05:24
 */
public class example {

    public static void main(String[] args) {
        quchon();
    }


    /***
    * 去重
    * @param
    * @return
    * @throws
    */
    public static void quchon(){

        List<String> zxOfficeList = new ArrayList<>();
        zxOfficeList.add("a");
        zxOfficeList.add("a");
        zxOfficeList.add("b");
        zxOfficeList.add("b");
        zxOfficeList.add("c");
        zxOfficeList.add("c");
        System.out.println(zxOfficeList);
        List<String> newList = new ArrayList<>(new HashSet<>(zxOfficeList));
        System.out.println(newList);




    }
    /***
     *  遍历消息
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:15:38
     */
    public static void ergodic() {
        List<String> list = Arrays.asList("apple", "banana", "orange");
        // 日常写法
        for (String s : list) {
            System.out.println(s);
        }
        // lambda表达式简写
        list.forEach(System.out::println);
        // 本质
        list.forEach(s -> {
            System.out.println(s);
        });
    }

    /***
     * @description lambda排序
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:20:12
     */
    public static void sort() {
        List<String> list = Arrays.asList("2", "3", "1");
        // 日常写法
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        // lambda表达式方式
        list.sort(String::compareTo);
        // 本质
        Collections.sort(list, (a, b) -> {
            return a.compareTo(b);
        });
        System.out.println(list);

    }



    /***
     * @description 过滤
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void With() {
        List<String> list = Arrays.asList("apple", "banana", "orange");
        // 第一个字段是不是a开头  startWith
        List<String> list2 = new ArrayList<>();
        for (String s : list) {
            if (s.startsWith("a")) {
                list2.add(s);
            }
        }
        List<String> list3 = list.stream().filter(s -> s.startsWith("a")).collect(Collectors.toList());
        System.out.println(list2);
        System.out.println(list3);
    }

    /***
     * @description 映射
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void yinshe() {
        List<String> list = Arrays.asList("apple", "banana", "orange");
        List<Integer> list2 = new ArrayList<>();
        for (String s : list) {
            list2.add(s.length());
        }
        List<Integer> list3 = list.stream().map(String::length).collect(Collectors.toList());
        List<Integer> list4 = list.stream().map(s -> {
            return s.length();
        }).collect(Collectors.toList());
        System.out.println(list2);
        System.out.println(list4);
    }

    /***
     * @description 规约
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void guiyue() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
        int sum = 0;
        for (Integer v : list) {
            sum += v;
        }
        System.out.println(sum);
        int sum2 = list.stream().reduce(0, (a, b) -> a + b);
        System.out.println(sum2);
    }

    /***
     * @description 分组
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void group() {
        List<String> list = Arrays.asList("apple", "banana", "orange");
        // 根据长度分组
        Map<Integer, List<String>> groups = new HashMap<>();
        for (String s : list) {
            int length = s.length();
            if (!groups.containsKey(length)) {
                groups.put(length, new ArrayList<>());
            }
            groups.get(length).add(s);
        }
        System.out.println(groups);
        // lambda表达式
        Map<Integer, List<String>> groups2 = list.stream().collect(Collectors.groupingBy(String::length));
        System.out.println(groups2);
    }


    /***
     * @description 接口式函数
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void myInterface() {
        MyInterface myInterface = new MyInterface() {
            @Override
            public void doSomething(String s) {
                System.out.println(s);
            }
        };
        myInterface.doSomething("hello world !!!");

        MyInterface myInterface1 = (s) -> System.out.println(s);
        myInterface1.doSomething("hello world !!!!!!");
    }

    /***
     * @description 创建线程
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void mythread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("helle Motor");
            }
        });
        thread.start();

        Thread thread1 = new Thread(() -> System.out.println("hello motor"));
        thread1.start();
    }

    /***
     * @description 判空操作
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void isnull() {
        String str = "hello motor";
        if (str != null){
            // 转大写
            System.out.println(str.toUpperCase());
        }
        Optional.ofNullable(str).map(String::toUpperCase).ifPresent(System.out::println);
    }


    /***
     * @description 流水线
     *
     * @return void
     * @author wz
     * @date 2023/8/27 14:28:12
     */
    public static void liushuixian() {
        List<String> list = Arrays.asList("apple", "banana", "orange","aaaa","abcs","azzzzzzzz");
        List<String> list2 = new ArrayList<>();
        // 判断是否是a开头，是 - 就把它添加到list2，并且转大写
        for(String s : list){
            if (s.startsWith("a")){
                list2.add(s.toUpperCase()); }
        }
        Collections.sort(list2);
        List<String> list3 = list.stream().filter(s -> s.startsWith("a")).
                map(String::toUpperCase).sorted().collect(Collectors.toList());
        System.out.println(list2);
        System.out.println(list3);
    }


}

interface MyInterface {
    public void doSomething(String s);
}
